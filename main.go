package main

import (
	"bytes"
	"flag"
	"io/ioutil"
	"log"
	"os"
	"os/signal"
	"strings"
	"syscall"
	"text/template"
)

type envFlags []string

func (ef *envFlags) String() string { return "" }

func (ef *envFlags) Set(value string) error {
	value = strings.TrimSpace(value)
	if strings.HasPrefix(value, "[") && strings.HasSuffix(value, "]") {
		for _, s := range strings.Split(value[1:len(value)-1], ",") {
			*ef = append(*ef, s)
		}
	} else {
		*ef = append(*ef, value)
	}
	return nil
}

func main() {
	log.SetFlags(log.LstdFlags | log.Lshortfile)

	var envfile = flag.String("envfile", "", "load environment variables from file")
	var env envFlags
	flag.Var(&env, "env", "set an environment in form K=V")
	flag.Parse()
	// log.Printf("env: %#v", env)

	if envfile != nil {
		env, err := ioutil.ReadFile(*envfile)
		if err != nil {
			log.Fatal(err)
		}

		for _, line := range bytes.Split(env, []byte{'\n'}) {
			line = bytes.TrimSpace(line)
			// skip empty lines
			if len(line) == 0 {
				continue
			}
			// skip comment lines
			if line[0] == '#' {
				continue
			}
			// remove end-line commnets
			parts := bytes.SplitN(line, []byte{'#'}, 2)
			line = parts[0]

			kv := bytes.SplitN(line, []byte{'='}, 2)
			// log.Printf("kv:%#q", kv)
			k := strings.TrimSpace(string(kv[0]))
			v := strings.TrimSpace(string(kv[1]))
			// log.Printf("k:%q, v:%q", k, v)

			err = os.Setenv(k, v)
			if err != nil {
				log.Fatal(err)
			}
		}
	}

	var input []byte
	var err error

	if flag.NArg() != 1 {
		input, err = ioutil.ReadAll(os.Stdin)
		if err != nil {
			log.Fatal(err)
		}
	} else {
		var filename = flag.Arg(0)
		input, err = ioutil.ReadFile(filename)
		if err != nil {
			log.Fatal(err)
		}
	}

	var funcs = template.FuncMap{"env": os.Getenv}
	var t = template.Must(template.New("input").Funcs(funcs).Parse(string(input)))

	err = t.Execute(os.Stdout, nil)
	signal.Ignore(syscall.SIGPIPE)
	if err != nil {
		log.Fatal(err)
	}
}
